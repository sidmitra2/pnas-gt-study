#!/usr/bin/perl

my $db=$ARGV[0];
my $list=$ARGV[1];

my %hash;
get_hash($db,\%hash);

my $hold =" \t \t \t \t \t";

open(LIST,"$list")||die "Can't open $list:$!\n";
<LIST>;
while(<LIST>)
{
    chomp;
    chop; #may need to this for ITB2A file but very strang
    my($type,$chr,$pos,$codon,$filter,$Ann,$AC,$AN,$NH,$AF) = split(/\t/);

    my $line =join("\t",$codon,$chr,$pos,$AF,$type);
    $type=~/^(\d+):(\d+)\s([ACGT])\s\/\s([ACGT])(.*)$/;
    my $ref = $3;
    my $alt = $4;
    my $dbsnp = $5; 

    print STDERR "!!!$ref,$alt,$pos\n";

    if (exists $hash{$pos})
    {
	my %record = %{$hash{$pos}};
	if(exists $record{$alt})
	{
	    my $score = $record{$alt};
	    print STDOUT "$line\t$score\n";

	}
	else
	{
	    print STDERR "something wrong2: $line\n";
	    print STDOUT "$line\n";
	    foreach $key (keys %record)
	    {
		#print STDERR "key:$key\n";
		my $score = $record{$key};
		print STDOUT "$hold\t$score\n";
	    }
	}
    }
    else
    {
	print STDERR "something wrong1: $line\n";
    }
    #die "Hello\n";
}
close(LIST);

sub get_hash($$)
{
    my ($file,$hash_ref) =@_;

    open(DB,"$file") || die "Can't open $file:$!\n";
    while(<DB>)
    {
	chomp;
	my $line =$_;
	my($chr,$pos,$ref,$alt,$aaref,$aaalt,@vals) = split(/\t/);
	$hash_ref->{$pos}{$alt} =$line;
    }
    close(DB);
}
